<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	function __construct(){
		parent::__construct(); //menggunakan konstruktor yg ada pada CI_Controller
		//panggil model m_mahasiswa
		$this->load->model('m_mahasiswa'); //fungsi model dari CI controller
	}

	public function index()
	{
		//ambil data
		$data['mahasiswa']=$this->m_mahasiswa->daftar();
		$this->load->view('components/header');
		$this->load->view('mahasiswa/list');
		$this->load->view('components/footer');
	}

	public function add()
	{
		$this->load->view('components/header');
		$this->load->view('mahasiswa/add');
		$this->load->view('components/footer');
	}
	
	
	public function save()
	{
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
		);
	$this->m_mahasiswa->simpan($data);
	redirect(base_url('index.php'));	
	}

	public function delete($id){
		$where = array(
			'id' => $id,
		);	
	$this->m_mahasiswa->hapus($where);
	redirect(base_url('index.php'));
	}
	public function detail()
	{
		$where=array(
			'id'=> $id,
		);
		$data['mahasiswa']=$this->m_mahasiswa->detail($where)->row();

		$this->load->view('components/header');
		$this->load->view('mahasiswa/detail',$data);
		$this->load->view('components/footer');
	}
	public function edit()
	{
		$where=array(
			'id'=> $id,
		);
		$data['mahasiswa']=$this->m_mahasiswa->edit($where)->row();

		$this->load->view('components/header');
		$this->load->view('mahasiswa/edit',$data);
		$this->load->view('components/footer');
	}

	public function update()
	{
		$where=array(
			'id'=> $id,
		);
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
		);
		$this->m_mahasiswa->ubah($where,$data);
		redirect(base_url('index.php'));	
	}
}	

